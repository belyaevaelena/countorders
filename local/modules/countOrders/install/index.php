<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Order\CountOrders\ModuleAgents;

Loc::loadMessages(__FILE__);

if (class_exists('countOrders')) {
    return;
}

Class countOrders extends CModule
{
    public function __construct()
    {
        $arModuleVersion = array();
        
        include __DIR__ . '/version.php';
		
        if (is_array($arModucountOrderModuleleVersion) && array_key_exists("VERSION", $arModuleVersion))
        {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }

		$this->MODULE_ID = 'countOrder';
        $this->MODULE_NAME = Loc::getMessage('COUNT_ORDER_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('COUNT_ORDER_MODULE_DESCRIPTION');
        $this->MODULE_GROUP_RIGHTS = 'N';
        $this->PARTNER_NAME = Loc::getMessage('COUNT_ORDER_MODULE_PARTNER_NAME');
        $this->PARTNER_URI = '';
    }
    function DoInstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION;
        
        $APPLICATION->IncludeAdminFile("Установка модуля countOrderModule", $DOCUMENT_ROOT."/local/modules/countOrders/install/step.php");
        ModuleManager::registerModule($this->MODULE_ID);
		\CAgent::AddAgent( "ModuleAgents::CheckOrderCount();", "countOrders", "Y", 3600, "", "Y");
		return true;
    }
    function DoUninstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION;
        ModuleManager::unRegisterModule($this->MODULE_ID);
		\CAgent::RemoveModuleAgents("countOrder");
        $APPLICATION->IncludeAdminFile("Деинсталляция модуля countOrderModule", $DOCUMENT_ROOT."/local/modules/countOrders/install/unstep.php");
        return true;
    }
	
}