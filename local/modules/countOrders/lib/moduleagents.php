<?php

namespace Order\CountOrders;
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

Class ModuleAgents {

	function CheckOrderCount(){
		$date = new DateTime();
		$date = \Bitrix\Main\Type\DateTime::createFromTimestamp($date->getTimestamp()); 
		$lastDate = COption::GetOptionString("main", "last_date_agent_checkOrderCount");//последнее выполнение агента
		if(\Bitrix\Main\Loader::includeModule('sale')){
		
			if ($lastDate) {
				$arFilter = array(">=DATE_INSERT" => $lastDate);
			}else{
				$arFilter = array(">=DATE_INSERT" => $date);
			}
			$parameters = [
				'select' => array('ID','SUM'),
				'filter' => $arFilter,
				'order' => ["DATE_INSERT" => "ASC"]
			];

			$dbRes = \Bitrix\Sale\Order::getList($parameters);
			$count=0;
			$sum=0;
			while ($order = $dbRes->fetch())
			{
				$count+=$order['ID'];
				$sum+=$order['SUM'];
			}
			global $DOCUMENT_ROOT;
			$filename = $DOCUMENT_ROOT.'/orders.txt';

			$file = date('d.m.Y h:i:s').' ' . $count . $sum .' ';

			$f=fopen($filename,'w');
			fwrite($f,);
			fclose($f);
			
		
			COption::SetOptionString("main", "last_date_agent_checkOrderCount", $date->toString());
		}

		return "CheckOrderCount();";
	}
}
